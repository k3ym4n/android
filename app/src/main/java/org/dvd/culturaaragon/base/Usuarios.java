package org.dvd.culturaaragon.base;

/**
 * Created by k3ym4n on 18/12/2016.
 */
public class Usuarios {

    private int id;
    private String usuario;
    private String contrasena;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
