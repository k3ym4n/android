package org.dvd.culturaaragon.pantallas;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.dvd.culturaaragon.R;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class Registro extends Activity implements View.OnClickListener {

    boolean registro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button btregistro = (Button) findViewById(R.id.btregistro);
        btregistro.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        EditText etRlogin = (EditText) findViewById(R.id.etRnombre);
        EditText etRpass = (EditText) findViewById(R.id.etRpass);


        switch (v.getId()) {

            case R.id.btregistro:
                String usuario = etRlogin.getText().toString();
                String contrasena = etRpass.getText().toString();
                WebService webService = new WebService();
                webService.execute(usuario, contrasena);

                break;
        }

    }

    private class WebService extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            try{
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getForObject
                        ("http://10.0.2.2:8080" + "/add_usuario?nombre=" + params[0] + "&contrasena=" + params[1], Void.class);
            }catch(Exception io){
                io.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);


        }
    }
}
