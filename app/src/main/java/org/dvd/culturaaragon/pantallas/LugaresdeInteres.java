package org.dvd.culturaaragon.pantallas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.dvd.culturaaragon.R;
import org.dvd.culturaaragon.adapter.AdapterLugaresDeInteres;
import org.dvd.culturaaragon.base.Lugares;
import org.dvd.culturaaragon.util.Constantes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class LugaresdeInteres extends AppCompatActivity {

    private AdapterLugaresDeInteres adapter;
    private ArrayList<Lugares> listalugares=new ArrayList<>();
    private ListView lvLugares;
    private String[] getLat,getLong;
    private String lat,lon;
    private Lugares lugaress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lugaresdeinteres);
        lvLugares = (ListView) findViewById(R.id.lvlugaresdeinteres);

        adapter = new AdapterLugaresDeInteres(this,listalugares);
        lvLugares.setAdapter(adapter);

       // registerForContextMenu(lvLugares);

        lvLugares.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("POSICION"+String.valueOf(position));
                for(int i=0;i<=getLong.length;i++){
                    lat=getLat[position];
                    lon=getLong[position];
                    System.out.println("LAT"+String.valueOf(lat).toString());
                    System.out.println("LONG"+String.valueOf(lon).toString());
                }

            }
        });

        try {
            new TareaDescarga().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }



    private class TareaDescarga extends AsyncTask<String,Void,Void>{

        private boolean error = false;
        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {

            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;


            try {
                URL urll = new URL(Constantes.URL);
                HttpURLConnection conexion = (HttpURLConnection) urll.openConnection();
                // Lee el fichero de datos y genera una cadena de texto como resultado
                BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null)
                    sb.append(linea + "\n");

                conexion.disconnect();
                br.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONObject("response").getJSONArray("docs");

                String titulo = null;
                String provincia = null;
                String municipio = null;
                String longitud = null;
                String latitud = null;
                String coordenadas = null;

                Lugares lugar = null;

                for (int i = 0; i < jsonArray.length(); i++) {


                    titulo = jsonArray.getJSONObject(i).getString("title");
                    try {
                        provincia = jsonArray.getJSONObject(i).getString("provincia");
                    }catch(JSONException j){
                        lugar.setProvincia("No se sabe");
                        j.printStackTrace();
                    }
                    municipio = jsonArray.getJSONObject(i).getString("municipio");
                    try{
                        coordenadas = jsonArray.getJSONObject(i).getString("location");
                    }catch(JSONException jse){
                        lugar.setLongitud(51.667851);
                        lugar.setLatitud(-34.885686 );
                        jse.printStackTrace();
                    }


                    coordenadas = coordenadas.substring(1,coordenadas.length() - 1);

                    String  latlong [] = coordenadas.split(",");
                        lugar = new Lugares();
                        lugar.setTitulo(titulo);
                        lugar.setProvincia(provincia);
                        lugar.setMunicipio(municipio);
                        lugar.setLatitud(Float.parseFloat(latlong[0]));
                        lugar.setLongitud(Float.parseFloat(latlong[1]));




                        //TODO intentar pasar la imagene si funciona bbb sino a cascarla

                        listalugares.add(lugar);
                }

            } catch (IOException e) {
                e.printStackTrace();
                error = true;
            } catch (JSONException e) {
                e.printStackTrace();
                error = true;
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            adapter.clear();
            listalugares = new ArrayList<Lugares>();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            adapter.notifyDataSetChanged();
        }

      /*  @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(LugaresdeInteres.this);
            dialog.setTitle(R.string.mensaje_cargando);
            dialog.show();
        }*/

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
         /*  if (error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.mensaje_error), Toast.LENGTH_SHORT).show();
                return;
            }

            if (dialog != null) {
                dialog.dismiss();
            }
            adapter.notifyDataSetChanged();*/
        }
    }



}



