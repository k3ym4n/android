package org.dvd.culturaaragon.pantallas;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.dvd.culturaaragon.R;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class Login extends Activity implements View.OnClickListener{

    boolean logearse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btlogin = (Button) findViewById(R.id.btlogin);
        Button btregistrar = (Button) findViewById(R.id.btregistrarse);
        btlogin.setOnClickListener(this);
        btregistrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        EditText etusuario = (EditText) findViewById(R.id.etusuairo);
        EditText etcontrasena = (EditText) findViewById(R.id.etcontrasena);

        switch(v.getId()){

            case R.id.btlogin:
                String usuario = etusuario.getText().toString();
                String contrasena = etcontrasena.getText().toString();
                WebService webService=new WebService();
                webService.execute(usuario,contrasena);

                break;
            case R.id.btregistrarse:

                Intent intent =  new Intent(this,Registro.class);
                startActivity(intent);
                break;
        }
    }

    private class WebService extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... params) {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            logearse = restTemplate.getForObject("http://10.0.2.2:8080" + "/usuario_login?nombre=" + params[0] + "&contrasena=" + params[1],Boolean.class);
            System.out.println(logearse);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(!logearse){
                Toast toast = Toast.makeText(getApplicationContext(), R.string.errorlogin,Toast.LENGTH_SHORT);
                toast.show();
            }else{

                Intent intent = new Intent(getApplicationContext(), LugaresdeInteres.class);
                startActivity(intent);
            }
        }
    }
}
