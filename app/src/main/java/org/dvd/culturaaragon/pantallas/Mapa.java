package org.dvd.culturaaragon.pantallas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.dvd.culturaaragon.R;
import org.dvd.culturaaragon.base.Lugares;
import org.dvd.culturaaragon.util.Constantes;
import org.dvd.culturaaragon.util.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Mapa extends Activity {

    private MapView mapView;
    private ArrayList<Lugares> listaLugares;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        listaLugares = new ArrayList<Lugares>();
        datosLugares();

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                for(Lugares lugares:listaLugares){
                    mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Util.parse(lugares.getLatitud(),lugares.getLongitud())))
                    .title(lugares.getTitulo())
                    .snippet(lugares.getDireccion()));
                }
            }
        });
    }

    private void datosLugares() {
        TareaDescarga tarea = new TareaDescarga();
        tarea.execute(Constantes.URL);
    }

    private class TareaDescarga extends AsyncTask<String, Void, Void> {

        private boolean error = false;
        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {

            String url = params[0];
            InputStream is = null;
            String result = null;
            JSONObject json = null;
            JSONArray jsonArray = null;


            try {
                HttpClient cliente = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                HttpResponse respuesta = cliente.execute(post);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null) {

                    sb.append(linea + "\n");
                }
                is.close();
                result = sb.toString();

                json = new JSONObject(result);
                jsonArray = json.getJSONArray("docs");

                //String titulo,provincia,municipio,telefono,descripcion,web;

                String titulo = null;
                Lugares lugar = null;

                for (int i = 0; i < jsonArray.length(); i++) {
                    titulo = jsonArray.getJSONObject(i).getString("title");

                    lugar = new Lugares();
                    lugar.setTitulo(titulo);

                    listaLugares.add(lugar);
                }


            } catch (IOException e) {
                e.printStackTrace();
                error = true;
            } catch (JSONException e) {
                e.printStackTrace();
                error = true;
            }

            return null;
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
            listaLugares = new ArrayList<Lugares>();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Mapa.this);
            dialog.setTitle(R.string.mensaje_cargando);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.mensaje_error), Toast.LENGTH_SHORT).show();
                return;
            }

            if (dialog != null) {
                dialog.dismiss();
            }

        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
