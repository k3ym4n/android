package org.dvd.culturaaragon.util;

import com.mapbox.mapboxsdk.geometry.LatLng;

/**
 * Created by k3ym4n on 18/12/2016.
 */
public class Util {

    public static uk.me.jstott.jcoord.LatLng DeUMTSaLatLN(double lat,double longi){
        uk.me.jstott.jcoord.UTMRef utm= new uk.me.jstott.jcoord.UTMRef(lat,longi,'N',30);
        return utm.toLatLng();
    }

    public static LatLng parse(double lat, double longi) {
        uk.me.jstott.jcoord.LatLng ubicacion=Util.DeUMTSaLatLN(lat,longi);
        return new LatLng(ubicacion.getLat(),ubicacion.getLng());
    }
}
