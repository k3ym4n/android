package org.dvd.culturaaragon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.dvd.culturaaragon.R;
import org.dvd.culturaaragon.base.Lugares;

import java.util.ArrayList;

/**
 * Created by k3ym4n on 18/12/2016.
 */
public class AdapterLugaresDeInteres extends ArrayAdapter<Lugares>{

    private final ArrayList<Lugares> lista;
    private final Context context;
    private LayoutInflater layoutInflater;
    private Lugares lugares;


    public AdapterLugaresDeInteres(Context context, ArrayList<Lugares> lista) {
        super(context,0, lista);
        this.context = context;
        this.lista = lista;
        this.layoutInflater=LayoutInflater.from(context);
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;


        if(view == null){
            view = layoutInflater.inflate(R.layout.fila_lugares,null);

            viewHolder = new ViewHolder();
            viewHolder.imagen = (ImageView) view.findViewById(R.id.ivfilalugares);
            viewHolder.titulo = (TextView) view.findViewById(R.id.tvfilatitulo);
            viewHolder.municipio = (TextView) view.findViewById(R.id.tvfilamunicipio);
            viewHolder.provincia = (TextView) view.findViewById(R.id.tvprovincia);

            view.setTag(viewHolder);


        }else{

            viewHolder = (ViewHolder) view.getTag();

        }
            lugares = lista.get(position);
            viewHolder.imagen.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_museo));
            viewHolder.titulo.setText(lugares.getTitulo());
            viewHolder.municipio.setText("Municipio: " + lugares.getMunicipio());
            viewHolder.provincia.setText("Provincia: " + lugares.getProvincia());

        return view;

    }

    static public class ViewHolder{

        ImageView imagen;
        TextView titulo;
        TextView municipio;
        TextView provincia;

    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Lugares getItem(int position) {
        return lista.get(position);
    }
    @Override
    public long getItemId(int i) {
        return i;
    }
}
